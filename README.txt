DRUPAL 7 EMPLOYEE FILE SYSTEM
-----------------------------

 * About D7 EFS
 * Installation
 * Configuration
 * Contrib modules used
 * Theme
 * Support
 * Test users

ABOUT EFS
----------

The Employee File System is a simple HRIS system that handles filing of:

* Employee 201 File
* Announcements
* Job Openings
* Vacation/Sick Leave
* Overtime
* Undertime
* Service Requests

EFS was created by RedJumpsuit (e: myredjumpsuit@gmail.com, w: http://redjumpsuit.net)

INSTALLATION
------------

- Download Drupal (tested working on v7.14 to v7.20 only)
- Replace Drupal /sites with EFS /sites
- Create the database using the included SQL file
- Update /sites/default/settings.php with your database configuration

TEST USERS
----------

* root/root (admin)
* jane.hr/123456 (human resource)
* john.doe/123456 (employee)

CONFIGURATION
-------------

- Update the file system config in Admin to point to your own temp folder
- Term references (like job types, leave types etc.) can be updated in the Taxonomy page
- Content permission can be modified by going to Content Types -> Select content type -> Edit -> Access Control

CONTRIB MODULES USED
--------------------

* acl
* administerusersbyrole
* better_formats
* content_access
* ctools
* date
* entity
* field_permission
* menu_per_role
* private_files_download_permission (slightly modified, see Line 121 to Line 125 on .module)
* references
* rules (disabled, but was intending to use it for notifications)
* views

THEME
-----

Theme used in EFS is a child Garland theme. You can find it located at /sites/all/themes/garland2


